from collections import namedtuple
from pprint import pprint
import re

Token = namedtuple('Token', ['value', 'type'])

re_number = r'(?P<number>(\d+|[-]\d+|\d[.]\d+))'
re_string = r'(?P<string>(\w+|"\w+"))'
re_stringComplexa = r'(?P<stringComplexa>("[\w+\W+]+?"|"[\W+\w+]+?"))'
re_operator = r'(?P<operator>\W)'
re_list = [
	re_number,
	re_string,
	re_stringComplexa,
	re_operator,
]
re_full = '|'.join(re_list)
re_compiled = re.compile(re_full)

def lexer(src: str) -> list:
	"""
	Lê uma string de código fonte JSON e retorna a lista de Tokens 
	correspondente.

	Em caso de erros, levanta uma exceção do tipo SyntaxError.
	"""
	tokens = []
	last_char = 0

	for m in re_compiled.finditer(src):
		i, j = m.span()

		last_char = j

		tk_type = m.lastgroup
		tk_value = src[i:j]
        
		if tk_value != ' ':
			if tk_type == 'stringComplexa':
				tk_type = 'string'

			if tk_value == '!' or tk_value == 'True' or tk_value == 'False':
				raise SyntaxError("invalid token!")

			if tk_value == 'true' or  tk_value == 'false' or tk_value == 'null':
				tk_type = 'constant'

			if tk_value == '[':
				tk_type = 'lbrack'

			if tk_value == ']':
				tk_type = 'rbrack'

			if tk_value == '{':
				tk_type = 'lbrace'

			if tk_value == '}':
				tk_type = 'rbrace'

			if tk_value == ',':
				tk_type = 'comma'

			if tk_value == ':':
				tk_type = 'colon'


			token = Token(tk_value, tk_type)
			tokens.append(token)

	return tokens

#while True:
#	pprint(lexer(input('Expr: ')))
